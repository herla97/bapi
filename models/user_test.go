package models

import "log"

func (ms *ModelSuite) Test_User_Create() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	u := &User{
		Username: "test1",
		Email:    "test1@test.com",
		Phone:    "1234567890",
		Password: "test1",
		Books: Books{
			Book{
				Title: "La Fundación 1",
				Isbn:  "ADN1",
			},
			Book{
				Title: "La Fundación 2",
				Isbn:  "ADN2",
			},
		},
	}

	verrs, err := u.Create(ms.DB)
	log.Println(verrs)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}
