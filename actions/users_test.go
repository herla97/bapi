package actions

import (
	"bapi/models"
	"net/http"
)

func (as *ActionSuite) CreateUser() *models.User {
	u := &models.User{
		Username: "test",
		Email:    "test@test.com",
		Phone:    "1234567890",
		Password: "testing",
		Books: models.Books{
			models.Book{
				Title: "La Fundación 1",
				Isbn:  "ADN1",
			},
			models.Book{
				Title: "La Fundación 2",
				Isbn:  "ADN2",
			},
		},
	}

	as.NoError(as.DB.Create(u))
	as.NotZero(u.ID)
	return u
}

// Other example using Fixtures
// func (as *ActionSuite) Test_UsersResource_List() {
// 	as.LoadFixture("bapi testing")

// 	res := as.JSON("/api/v1/users").Get()

// 	body := res.Body.String()
// 	as.Contains(body, "saherla")
// }

func (as *ActionSuite) Test_UsersResource_List() {
	res := as.JSON("/api/v1/users").Get()
	as.Equal(http.StatusOK, res.Code)
}

func (as *ActionSuite) Test_UsersResource_Show() {
	u := as.CreateUser()

	res := as.JSON("/api/v1/users/%s", u.ID).Get()
	as.Equal(http.StatusOK, res.Code)
	as.Contains(res.Body.String(), u.Username)
}

// func (as *ActionSuite) Test_UsersResource_Update() {
// 	as.Fail("Not Implemented!")
// }

// func (as *ActionSuite) Test_UsersResource_Destroy() {
// 	as.Fail("Not Implemented!")
// }

// ---

// Not Works
// func (as *ActionSuite) Test_UsersResource_Create() {

// 	u := &models.User{
// 		Username: "test",
// 		Email:    "test@test.com",
// 		Phone:    "1234567890",
// 		Password: "testing",
// 		Books: models.Books{
// 			models.Book{
// 				Title: "La Fundación 1",
// 				Isbn:  "ADN1",
// 			},
// 			models.Book{
// 				Title: "La Fundación 2",
// 				Isbn:  "ADN2",
// 			},
// 		},
// 	}

// 	res := as.JSON("/api/v1/users").Post(u)

// 	err := as.DB.First(u)
// 	log.Println(u)
// 	as.NoError(err)
// 	as.NotZero(u.ID)

// 	as.Equal("test", u.Username)

// 	as.Equal(fmt.Sprintf("/api/v1/users/%s", u.ID), res.Location())
// }
