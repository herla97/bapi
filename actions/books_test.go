package actions

import "net/http"

func (as *ActionSuite) Test_BooksResource_List() {
	res := as.JSON("/api/v1/books").Get()
	as.Equal(http.StatusOK, res.Code)
}

// func (as *ActionSuite) Test_BooksResource_Show() {
// 	as.Fail("Not Implemented!")
// }
